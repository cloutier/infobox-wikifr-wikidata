use Data::Dump qw(dump);
use utf8;
use open ':std', ':encoding(UTF-8)';

use MongoDB;
use DBI;

my $Mongo = MongoDB::MongoClient->new( host => 'localhost', port => 27017 );
my $mongo = $Mongo->get_database("frwiki");

my $dsn = "DBI:mysql:database=frwiki;host=localhost;";
my $dbh = DBI->connect($dsn, "root", "password");

my $lang = $mongo->get_collection("pages")->find({"categories"=> "Inventaire de langues"});
while  (my $e = $lang->next ) {
	#print "scanning $e->{_id}\n";

    my $title = $e->{_id};
    my $qcode = convert_title_to_qcode($title);
    if (not defined $qcode) {
        next;
    }
    my $infobox = $e->{sections}[0]->{infoboxes}[0];
    my $ietf = $infobox->{ietf}->{text};
    if (defined $ietf and length($ietf)==3) {
        print qq($qcode|P305|"$ietf"\n);
    }
    my $iso1 = $infobox->{iso1}->{text};
    if (defined $iso1 and length($iso1)==2) {
        print qq($qcode|P218|"$iso1"\n);
    }
    my $iso2 = $infobox->{iso2}->{text};
    if (defined $iso2 and length($iso2)==3) {
        print qq($qcode|P219|"$iso2"\n);
    }
    my $iso3 = $infobox->{iso3}->{text};
    if (defined $iso3 and length($iso3)==3) {
        print qq($qcode|P220|"$iso3"\n);
    }
    my $glottolog = $infobox->{glottolog}->{text};
    if (defined $glottolog and length($glottolog)==8) {
        print qq($qcode|P1394|"$glottolog"\n);
    }
    my $wals = $infobox->{wals}->{text};
    if (defined $wals and length($wals)==3) {
        print qq($qcode|P1466|"$wals"\n);
    }
    my $lingua = $infobox->{lingua}->{text};
    if (defined $lingua and length($lingua)<11) {
        print qq($qcode|P1396|"$lingua"\n);
    }
	#print dump($infobox);
}

sub convert_title_to_qcode {
    my $title = $_[0];
    $title =~ s/ /_/g;
	my $sth = $dbh->prepare("SELECT page_title, pp_value FROM page LEFT JOIN page_props ON page_props.pp_page = page_id WHERE pp_propname='wikibase_item' AND page_title=? LIMIT 10;");
	$sth->execute($title);
	my @row = $sth->fetchrow_array();
	$sth->finish();
	return @row[1];
}
