use Data::Dump qw(dump);
use utf8;
use open ':std', ':encoding(UTF-8)';
use Curses;

use MongoDB;
use DBI;
use MediaWiki::API;

print Dumper \@ARGV;

my $Mongo = MongoDB::MongoClient->new( host => 'localhost', port => 27017 );
my $mongo = $Mongo->get_database("frwiki");

my $dsn = "DBI:mysql:database=frwiki;host=localhost;";
my $dbh = DBI->connect($dsn, "root", "password");

my $mw = MediaWiki::API->new();
$mw->{config}->{api_url} = 'https://fr.wikipedia.org/w/api.php';
$mw->login( { lgname => 'Vincent cloutier', lgpassword => $ARGV[0]  } )
  || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};

my $lang = $mongo->get_collection("pages")->find({"categories"=> "Inventaire de langues"});
while  (my $e = $lang->next ) {
    my $infobox = $e->{sections}[0]->{infoboxes}[0];
    my $title = $e->{title};
    my $ietf = $infobox->{ietf}->{text};
    my $iso1 = $infobox->{iso1}->{text};
    my $iso2 = $infobox->{iso2}->{text};
    my $iso3 = $infobox->{iso3}->{text};
    my $glottolog = $infobox->{glottolog}->{text};
    my $wals = $infobox->{wals}->{text};
    my $lingua = $infobox->{lingua}->{text};
    my $qcode = convert_title_to_qcode($title);

    print "Scanning $title\n";
    my $page = $mw->get_page( { title => $e->{title} } ); # print page contents
    $content = $page->{'*'};

    if (not defined $ietf or wikidata_contains_already($ietf, $qcode, "P305")) {
        $content =~ s/ *\|\s?ietf\s*=.*\n//g;
    }
    if (not defined $iso1 or wikidata_contains_already($iso1, $qcode, "P218")) {
        $content =~ s/ *\|\s?iso1\s*=.*\n//g;
    }
    if (not defined $iso2 or wikidata_contains_already($iso2, $qcode, "P219")) {
        $content =~ s/ *\|\s?iso2\s*=.*\n//g;
    }
    if (not defined $iso3 or wikidata_contains_already($iso3, $qcode, "P220")) {
        $content =~ s/ *\|\s?iso3\s*=.*\n//g;
    }
    if (not defined $glottolog or wikidata_contains_already($glottolog, $qcode, "P1394")) {
        $content =~ s/ *\|\s?glottolog\s*=.*\n//g;
    }
    if (not defined $wals or wikidata_contains_already($wals, $qcode, "P1466")) {
        $content =~ s/ *\|\s?wals\s*=.*\n//g;
    }
    if (not defined $lingua or wikidata_contains_already($lingua, $qcode, "P1396")) {
        $content =~ s/ *\|\s?lingua\s*=.*\n//g;
    }
    $content =~ s/ *\|\s?parlée\s*=.*\n//g;
    $content =~ s/ *\|\s?iso3-note\s*=.*\n//g;
    $content =~ s/ *\|\s?wals-note\s*=.*\n//g;
    $content =~ s/ *\|\s?iso5-note\s*=.*\n//g;
    $content =~ s/ *\|\s?glottolog-note\s*=.*\n//g;
    $content =~ s/ *\|\s?lingua-note\s*=.*\n//g;
    $content =~ s/ *\|\s?typologie\s*=.*\n//g;
    $content =~ s/ *\|\s?académie\s*=.*\n//g;

    if ($content ne $page->{'*'}) {

        print "Changes for $e->{title}\n";
        print "https://fr.wikipedia.org/?curid=$e->{pageID}\n";
        print "https://wikidata.org/wiki/$qcode\n";
        print_string_diff($content, $page->{'*'});
        print "\n";
        my $conf = prompt();
        if ($conf eq 'y') {
            print "do it";
            $mw->edit( {
                action => 'edit',
                summary => 'Déplacement de données vers Wikidata',
                title => $title,
                text => $content } )
                || die $mw->{error}->{code} . ': ' . $mw->{error}->{details};
        }
    }
}

sub convert_title_to_qcode {
    my $title = $_[0];
    $title =~ s/ /_/g;
	my $sth = $dbh->prepare("SELECT page_title, pp_value FROM page LEFT JOIN page_props ON page_props.pp_page = page_id WHERE pp_propname='wikibase_item' AND page_title=? LIMIT 10;");
	$sth->execute($title);
	my @row = $sth->fetchrow_array();
	$sth->finish();
	return @row[1];
}

sub convert_qcode_to_title {
	my $sth = $dbh->prepare("SELECT page_title, pp_value FROM page LEFT JOIN page_props ON page_props.pp_page = page_id WHERE pp_propname='wikibase_item' AND pp_value=? LIMIT 10;");
	$sth->execute($_[0]);
	my @row = $sth->fetchrow_array();
	$sth->finish();
	return @row[2];
}

sub wikidata_contains_already {
    return true;
}

sub print_string_diff {

    open(my $a, '>', '/tmp/a.txt');
    open(my $b, '>', '/tmp/b.txt');

    print $a $_[0];
    print $b $_[1];

    print `diff /tmp/b.txt /tmp/a.txt`;

}

sub prompt {
  my ($query) = @_; # take a prompt string as argument
  local $| = 1; # activate autoflush to immediately show the prompt
  print $query;
  chomp(my $answer = <STDIN>);
  return $answer;
}
