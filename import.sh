#!/bin/bash
set -e

date="20190720"

rm /mnt/blockchains/frwiki*

gsutil cp gs://concured-wikimedia-dumps/frwiki/$date/frwiki-$date-pages-articles.xml.bz2 /mnt/blockchains/
gsutil cp gs://concured-wikimedia-dumps/frwiki/$date/frwiki-$date-page.sql.gz /mnt/blockchains/
gsutil cp gs://concured-wikimedia-dumps/frwiki/$date/frwiki-$date-page_props.sql.gz /mnt/blockchains/

time bzip2 -d /mnt/blockchains/frwiki-$date-pages-articles.xml.bz2
time gzip -d /mnt/blockchains/frwiki*.gz

echo "CREATE INDEX title ON page(page_title);" | mysql --user=root --password=password -b frwiki
time cat /mnt/blockchains/frwiki-$date-page_props.sql | mysql --user=root --password=password -b frwiki
time cat /mnt/blockchains/frwiki-$date-page.sql | mysql --user=root --password=password -b frwiki
dumpster /mnt/blockchains/frwiki-$date-pages-articles.xml --infoboxes=true --citations=false --categories=true --skip_disambig --skip_redirects
